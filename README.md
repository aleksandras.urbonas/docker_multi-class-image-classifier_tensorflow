```
# version: 2022-03-08T2218 AU
# review : 2022-03-08T2348 AU
```

1. Dataset:
	<https://drive.google.com/u/0/uc?id=1odE6Q2w-wSqnfO34fRbtQUmP3zr3M-eG&export=download>
	<https://drive.google.com/file/d/1odE6Q2w-wSqnfO34fRbtQUmP3zr3M-eG/view>

2. Create directory in `Docker`:
	<https://stackoverflow.com/questions/42400290/create-files-folders-on-docker-compose-build-or-docker-compose-up>

3. Article:
	<https://www.analyticsvidhya.com/blog/2021/10/end-to-end-guide-to-docker-for-aspiring-data-engineers/>

4. Image Sources
	Image 1 – https://developers.redhat.com/blog/2014/05/15/practical-introduction-to-docker-containers
	Image 2 – https://www.docker.com/resources/what-container

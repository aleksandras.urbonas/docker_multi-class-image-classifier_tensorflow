﻿FROM tensorflow/tensorflow:latest-gpu

WORKDIR ./docker_training

COPY . .

RUN apt-get update

RUN python -m pip install --upgrade pip

RUN python -m pip install matplotlib==3.5.1
RUN python -m pip install numpy==1.22.1
RUN python -m pip install Pillow==9.0.1
RUN python -m pip install scikit-learn==1.0.2

ENTRYPOINT [ "python3", "train.py" ]
